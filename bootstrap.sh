#!/bin/bash -x
set -e
libtoolize --force
aclocal
autoconf
automake --add-missing
